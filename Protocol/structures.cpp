/**
 * Na serveri chceme uchovávať všetky informácie, ktoré
 * môže používateľ nastaviť.
 * 
 * Serveru je jedno, aké informácie uchováva, klient musí
 * poznať, aká štruktúra mu zo servera príde. Preto treba
 * dodržať maximálne veľkosti štruktúr.
 */

struct ant { 						
	unsigned char i, j;			
	unsigned char antType; // číslo
	unsigned char orientation;
};

struct antType
{
	unsigned char priority;
	bool logic; // false - PRIAMA, true - INVERTOVANÁ
	unsigned char color; // číslo
};

struct pattern {					
	unsigned char width, height;
	unsigned short antsCount;
	bool isBlack[50][50];
	ant ants[2500];
	antType antTypes[5];	// [podlaPoradiFarieb] invertovanie treba aj v GUI nastaviť
};