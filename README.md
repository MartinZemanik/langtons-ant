# Langton's Ant

This aplications consist of two parts - server and client.

## Server application

On server are stored simulation patterns. Server is accesible via client application.

Server will listen on any adress, but only on port provided as **first** (and only) program parameter. If no parameter is set, default port **11031** will be used.

## Client application

Created with framework [qt](https://www.qt.io/).

User can interact with simulation grid here. Application allows user to save/load simulation patterns from file or from server.

Repository include latest portable version of client application in [LangtonsAntClient](LangtonsAntClient_portable.zip) file.

---

Created with ❤️ for [FRI UNIZA](https://www.fri.uniza.sk/)!
