#pragma once
#include "ui_ServerConfigDialog.h"
#include <QDialog>
#include <QIntValidator>
#include "../utilities/ServerConfig.h"

class ServerConfigDialog : public QDialog
{
    Q_OBJECT

public:
    ServerConfigDialog(QWidget* parent);

protected:
    void showEvent(QShowEvent* ev);

private slots:
    void on_saveButton_clicked();
    void on_cancelButton_clicked();

private:
    Ui::ServerConfigDialog _ui;
};