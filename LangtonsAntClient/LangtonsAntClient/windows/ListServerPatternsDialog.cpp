#include "ListServerPatternsDialog.h"
#include <QIcon>
#include <QPushButton>

const QString ListServerPatternsDialog::PATTERN_FILE_NAME_EXTENSION = ".pattern";

ListServerPatternsDialog::ListServerPatternsDialog(QWidget* parent, std::function<void(const pattern* patt)> patternLoaded) :
    QDialog(parent),
    _loadedPatterns(new QStringList()),
    _patternLoaded(patternLoaded)
{
    _ui.setupUi(this);
    setWindowIcon(QIcon("icons/window-icon.png"));

    _patternsListModel = new QStringListModel(this);
    _ui.patternsListView->setModel(_patternsListModel);

    _requestFetchFileList = new RequestFetchFileList();
    connect(_requestFetchFileList, &ServerRequest::onSocketError, this, &ListServerPatternsDialog::onSocketError);
    connect(_requestFetchFileList, &RequestFetchFileList::fileListFetched, this, &ListServerPatternsDialog::fileListFetched);

    _requestGetPattern = new RequestGetPattern();
    connect(_requestGetPattern, &ServerRequest::onSocketError, this, &ListServerPatternsDialog::onSocketError);
    connect(_requestGetPattern, &RequestGetPattern::patternGettingDone, this, &ListServerPatternsDialog::patternGettingDone);

    _requestDeletePattern = new RequestDeletePattern();
    connect(_requestDeletePattern, &ServerRequest::onSocketError, this, &ListServerPatternsDialog::onSocketError);
    connect(_requestDeletePattern, &RequestDeletePattern::patternDeletingDone, this, &ListServerPatternsDialog::patternDeletingDone);
}

ListServerPatternsDialog::~ListServerPatternsDialog()
{
    delete _patternsListModel;
    delete _loadedPatterns;
    delete _requestFetchFileList;
    delete _requestGetPattern;
    delete _requestDeletePattern;
}

void ListServerPatternsDialog::showEvent(QShowEvent* ev)
{
    _ui.loadButton->setFocus();
    refreshFileList();
}

void ListServerPatternsDialog::on_refreshButton_clicked()
{
    refreshFileList();
}

void ListServerPatternsDialog::on_loadButton_clicked()
{
    int patternIndex = _ui.patternsListView->currentIndex().row();

    if (patternIndex != -1)
    {
        _requestGetPattern->run(
            ServerConfig::instance().getQStringAddress(),
            ServerConfig::instance().getPort(),
            ((*_loadedPatterns)[patternIndex] + PATTERN_FILE_NAME_EXTENSION)
        );

        enableButtons(false);
    }
    else
    {
        QMessageBox::critical(this, "Selection error",
            "No pattern selected.");
    }
}

void ListServerPatternsDialog::on_deleteButton_clicked()
{
    int patternIndex = _ui.patternsListView->currentIndex().row();

    if (patternIndex != -1)
    {
        QMessageBox::StandardButton reply = QMessageBox::question(
            this,
            "Delete pattern",
            "Do you really wish to delete pattern " + (*_loadedPatterns)[patternIndex] + "?",
            QMessageBox::Yes | QMessageBox::No);

        if (reply == QMessageBox::Yes)
        {
            _requestDeletePattern->run(
                ServerConfig::instance().getQStringAddress(),
                ServerConfig::instance().getPort(),
                ((*_loadedPatterns)[patternIndex] + PATTERN_FILE_NAME_EXTENSION)
            );

            enableButtons(false);
        }
    }
    else
    {
        QMessageBox::critical(this, "Selection error", "No pattern selected.");
    }
}

void ListServerPatternsDialog::onSocketError(QAbstractSocket::SocketError socketError)
{
    switch (socketError)
    {
    case QAbstractSocket::RemoteHostClosedError:
        break;
    case QAbstractSocket::HostNotFoundError:
        QMessageBox::critical(this, "Connection error",
            "The server was not found. Please check IP/Domain name and port number.");
        break;
    case QAbstractSocket::ConnectionRefusedError:
        QMessageBox::critical(this, "Connection error",
            "The server is unreachable, please try again later.");
        break;
    default:
        QMessageBox::critical(this, "Connection error",
            "Unknown network error occured. Try again later.");
    }

    enableButtons(true);
}

void ListServerPatternsDialog::fileListFetched(std::vector<QString> fileList)
{
    if (fileList.size() <= 0)
    {
        QMessageBox::information(this, "Fetch list request",
            "There are no patterns on the server.");
    }
    else {
        for (auto& patternName : fileList)
        {
            _loadedPatterns->push_back(patternName.left(patternName.size() - PATTERN_FILE_NAME_EXTENSION.size()));
        }

        _patternsListModel->setStringList(*_loadedPatterns);
    }

    enableButtons(true);
}

void ListServerPatternsDialog::patternGettingDone(bool found, const pattern* patt)
{
    if (found)
    {
        _patternLoaded(patt);
        hide();
    }
    else
    {
        QMessageBox::critical(this, "Getting pattern error",
            "Pattern not found on the server.");
    }

    enableButtons(true);
}

void ListServerPatternsDialog::patternDeletingDone(const char status)
{
    if (status == RequestDeletePattern::STATUS_SUCCESS)
    {
        refreshFileList();

        QMessageBox::information(this, "Pattern deleting done",
            "The pattern was successfully deleted from the server!");
    }
    else
    {
        QMessageBox::critical(this, "Pattern deleting error",
            "Error deleting pattern from the server!");

        enableButtons(true);
    }
}

void ListServerPatternsDialog::refreshFileList()
{
    _loadedPatterns->clear();
    _patternsListModel->setStringList(*_loadedPatterns);
    _requestFetchFileList->run(ServerConfig::instance().getQStringAddress(), ServerConfig::instance().getPort());

    enableButtons(false);
}

void ListServerPatternsDialog::enableButtons(bool enable)
{
    _ui.refreshButton->setEnabled(enable);
    _ui.loadButton->setEnabled(enable);
    _ui.deleteButton->setEnabled(enable);
}

