#pragma once
#include "ui_ListServerPatternsDialog.h"
#include <QWidget>
#include <QDialog>
#include <QStringListModel>
#include <vector>
#include <QMessageBox>
#include <QDebug>
#include <QString>
#include "../server/RequestFetchFileList.h"
#include "../server/RequestGetPattern.h"
#include "../server/RequestDeletePattern.h"
#include "../utilities/structures.h"
#include "../utilities/ServerConfig.h"

using std::vector;

class ListServerPatternsDialog : public QDialog
{
    Q_OBJECT

public:
    ListServerPatternsDialog(QWidget* parent, std::function<void(const pattern* patt)> patternLoaded);
    ~ListServerPatternsDialog();
    static const QString PATTERN_FILE_NAME_EXTENSION;

protected:
    void showEvent(QShowEvent* ev);

private slots:
    void on_refreshButton_clicked();
    void on_loadButton_clicked();
    void on_deleteButton_clicked();
    void onSocketError(QAbstractSocket::SocketError socketError);
    void fileListFetched(std::vector<QString> fileList);
    // Param 'patt' is allocated on the heap, handler has to free allocated memory.
    void patternGettingDone(bool found, const pattern* patt);
    void patternDeletingDone(const char status);

private:
    Ui::ListServerPatternsDialog _ui;

    QStringListModel* _patternsListModel;
    QStringList* _loadedPatterns;
    RequestFetchFileList* _requestFetchFileList;
    RequestGetPattern* _requestGetPattern;
    RequestDeletePattern* _requestDeletePattern;

    std::function<void(const pattern* patt)> _patternLoaded;

    void refreshFileList();
    void enableButtons(bool enable);
};
