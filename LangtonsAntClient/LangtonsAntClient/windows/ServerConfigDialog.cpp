#include "ServerConfigDialog.h"

ServerConfigDialog::ServerConfigDialog(QWidget* parent) :
    QDialog(parent)
{
    _ui.setupUi(this);
    setWindowIcon(QIcon("icons/window-icon.png"));

    _ui.portEdit->setValidator(new QIntValidator(1, 65535, this));
}

void ServerConfigDialog::showEvent(QShowEvent* ev)
{
    _ui.addressEdit->setText(ServerConfig::instance().getQStringAddress());
    _ui.portEdit->setText(ServerConfig::instance().getQStringPort());
    _ui.saveButton->setFocus();
    _ui.saveButton->setAutoDefault(true);
    _ui.saveButton->setDefault(true);
    _ui.cancelButton->setAttribute(Qt::WA_UnderMouse, false);
    _ui.saveButton->setAttribute(Qt::WA_UnderMouse, false);
}

void ServerConfigDialog::on_saveButton_clicked()
{
    ServerConfig::instance().setConfig(_ui.addressEdit->text().toStdString(), _ui.portEdit->text().toInt());
    hide();
}

void ServerConfigDialog::on_cancelButton_clicked()
{
    hide();
}
