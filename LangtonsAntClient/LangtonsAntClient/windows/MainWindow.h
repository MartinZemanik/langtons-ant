#pragma once
#include "ui_MainWindow.h"
#include "../ant/AntTypeWidget.h"
#include "../simulation/GridTileWidget.h"
#include "../simulation/SimulationGrid.h"
#include "ServerConfigDialog.h"
#include "ListServerPatternsDialog.h"
#include "../utilities/ServerConfig.h"
#include "../server/RequestSavePattern.h"
#include "../utilities/SimulationDataParser.h"
#include "../utilities/LocalFilesHandler.h"
#include <QMainWindow>
#include <QPushButton>
#include <QProgressDialog>
#include <vector>
#include <QString>
#include <QIntValidator>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget* parent = Q_NULLPTR);
    ~MainWindow();

private slots:
    void on_actionLoadPattern_file_triggered();
    void on_actionSavePattern_file_triggered();
    void on_actionQuit_triggered();
    void on_actionListPatterns_server_triggered();
    void on_actionSavePattern_server_triggered();
    void on_actionConfigServer_triggered();

    void on_applyDimButton_clicked();

    void on_manualSelectBlackTilesButton_clicked();
    void on_generateBlackTilesButton_clicked();
    void on_generateAntsButton_clicked();
    void on_conflictsCheckbox_clicked();

    void on_nextStepButton_clicked();
    void on_startButton_clicked();
    void on_pauseButton_clicked();
    void on_resetButton_clicked();

    void on_refreshTimeDecButton_clicked();
    void on_refreshTimeIncButton_clicked();

    void onSocketError(QAbstractSocket::SocketError socketError);
    void patternSavingDone(const char status);

private:
    void updateSimulationGrid();
    void gridTileClicked(int i, int j);
    void antEmplaceClicked(int colorIndex, bool emplacing);

    void updateManualSelectBlackTilesButtonColor();
    void updateBlackTilesLabel();
    void updateRefreshTimeText();
    void updateIterationText();

    void showLoadingDialog();
    void hideLoadingDialog();

    void patternLoadedFromServer(const pattern* patt);

    const std::vector<QString> ANTS_COLORS = { "red", "green", "blue", "orange", "yellow" };

    Ui::LangtonsAntClientClass _ui;

    SimulationGrid* _simulationGrid;

    std::vector<AntTypeWidget*>* _antTypesWidgets;

    bool _isEditingBlackTiles;
    AntTypeWidget* _emplacingAntWidget;

    ServerConfigDialog* _serverConfigDialog;
    ListServerPatternsDialog* _listServerPatternsDialog;

    RequestSavePattern* _requestSavePattern;
    QProgressDialog* _loadingDialog;
};
