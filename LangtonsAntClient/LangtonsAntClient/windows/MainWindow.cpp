#include "MainWindow.h"
#include "../utilities/ServerConfig.h"
#include <QObject>
#include <QInputDialog>
#include <QFileDialog>
#include <iostream>
#include <utility>
#include <vector>

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent),
    _antTypesWidgets(new std::vector<AntTypeWidget*>()),
    _isEditingBlackTiles(false),
    _emplacingAntWidget(nullptr),
    _serverConfigDialog(new ServerConfigDialog(this)),
    _listServerPatternsDialog(new ListServerPatternsDialog(this, [this](const pattern* patt) { patternLoadedFromServer(patt); }))
{
    setWindowIcon(QIcon("icons/window-icon.png"));

    _ui.setupUi(this);
    _simulationGrid = new SimulationGrid(this, _ui.simulationGrid, [this](int i, int j) {gridTileClicked(i, j); }, 100, [this]() {updateBlackTilesLabel(); }, _antTypesWidgets, [this]() {updateIterationText(); });
    updateSimulationGrid();

    _ui.blackTilesToGenerate->setValidator(new QIntValidator(1, 2500, this));
    _ui.rowsTextEdit->setValidator(new QIntValidator(1, 50, this));
    _ui.colsTextEdit->setValidator(new QIntValidator(1, 50, this));

    _ui.manualSelectBlackTilesButton->setIcon(QIcon("icons/pencil.png"));
    updateManualSelectBlackTilesButtonColor();

    updateBlackTilesLabel();
    updateRefreshTimeText();
    updateIterationText();

    for (int i = 0; i < ANTS_COLORS.size(); i++)
    {
        AntType* newAntType = new AntType(ANTS_COLORS[i], AntLogic::NORMAL, 0);
        AntTypeWidget* newAntTypeWidget = new AntTypeWidget(this, _ui.antsLayout, i + 1, newAntType, [this, i](bool emplacing) {antEmplaceClicked(i, emplacing); });
        _antTypesWidgets->push_back(newAntTypeWidget);
        _ui.antsLayout->addWidget(newAntTypeWidget);
    }

    _requestSavePattern = new RequestSavePattern();
    connect(_requestSavePattern, &ServerRequest::onSocketError, this, &MainWindow::onSocketError);
    connect(_requestSavePattern, &RequestSavePattern::patternSavingDone, this, &MainWindow::patternSavingDone);

    _loadingDialog = nullptr;
}

MainWindow::~MainWindow()
{
    for (unsigned int i = 0; i < _antTypesWidgets->size(); i++)
        delete (*_antTypesWidgets)[i];
    delete _antTypesWidgets;

    delete _simulationGrid;
    delete _serverConfigDialog;
    delete _listServerPatternsDialog;

    delete _requestSavePattern;
    if (_loadingDialog != nullptr)
        delete _loadingDialog;
}

void MainWindow::updateSimulationGrid()
{
    if (_ui.rowsTextEdit->text().isEmpty() || _ui.rowsTextEdit->text().toInt() <= 0 || _ui.rowsTextEdit->text().toInt() > SimulationGrid::MAX_SIZE ||
        _ui.colsTextEdit->text().isEmpty() || _ui.colsTextEdit->text().toInt() <= 0 || _ui.colsTextEdit->text().toInt() > SimulationGrid::MAX_SIZE)
    {
        QMessageBox::critical(this, "Invalid number", "Please enter valid dimensions (1 - 50).");
        return;
    }

    int rows = _ui.rowsTextEdit->text().toInt();
    int cols = _ui.colsTextEdit->text().toInt();

    _simulationGrid->resizeSimulationGrid(rows, cols);
    updateBlackTilesLabel();
    updateIterationText();
}

void MainWindow::gridTileClicked(int i, int j)
{
    if (_isEditingBlackTiles)
    {
        if (_simulationGrid->getTile(i, j)->isWhite())
            _simulationGrid->setBlackTiles(_simulationGrid->getCurrentBlackTiles() + 1);
        else
            _simulationGrid->setBlackTiles(_simulationGrid->getCurrentBlackTiles() - 1);
        updateBlackTilesLabel();

        _simulationGrid->getTile(i, j)->invertColor();
    }
    else if (_emplacingAntWidget != nullptr)
    {
        if (_simulationGrid->getTile(i, j)->getCurrentAnt() == nullptr)
        {
            _simulationGrid->addAnt(i, j, new Ant(i, j, 0, _emplacingAntWidget->getType()));
            _emplacingAntWidget->setCount(_emplacingAntWidget->getCount() + 1);
        }
        else
        {
            Ant* a = _simulationGrid->getTile(i, j)->getCurrentAnt();
            AntTypeWidget* clickedAntType = nullptr;
            for (unsigned int i = 0; i < _antTypesWidgets->size(); i++)
            {
                if ((*_antTypesWidgets)[i]->getType()->getColor() == a->getType()->getColor())
                {
                    clickedAntType = (*_antTypesWidgets)[i];
                    break;
                }
            }
            _simulationGrid->removeAnt(i, j);
            if (clickedAntType != nullptr) //Just to be sure. It should never be nullptr
                clickedAntType->setCount(clickedAntType->getCount() - 1);
        }
    }
}

void MainWindow::antEmplaceClicked(int colorIndex, bool emplacing)
{
    _isEditingBlackTiles = false;
    updateManualSelectBlackTilesButtonColor();

    for (unsigned int i = 0; i < _antTypesWidgets->size(); i++)
    {
        if (i != colorIndex)
            (*_antTypesWidgets)[i]->setIsEmplacing(false);
    }

    _emplacingAntWidget = emplacing ? (*_antTypesWidgets)[colorIndex] : nullptr;
}

void MainWindow::updateManualSelectBlackTilesButtonColor()
{
    QString newButtonColor = _isEditingBlackTiles ? "orange" : "white";
    _ui.manualSelectBlackTilesButton->setStyleSheet(QString("background-color: %1").arg(newButtonColor));
}

void MainWindow::updateBlackTilesLabel()
{
    _ui.currentBlackTilesLabel->setText(QString("Current black tiles: %1").arg(_simulationGrid->getCurrentBlackTiles()));
}

void MainWindow::updateRefreshTimeText()
{
    _ui.currentRefreshTimeLabel->setText(QString("%1").arg(_simulationGrid->getCurrentRefreshTime()));
}

void MainWindow::updateIterationText()
{
    _ui.currentIterationLabel->setText(QString("Iteration: %1").arg(_simulationGrid->getCurrentIteration()));
}

void MainWindow::showLoadingDialog()
{
    if (_loadingDialog != nullptr)
        delete _loadingDialog;

    _loadingDialog = new QProgressDialog("Loading, please hold a second...", "", 0, 100, this);
    _loadingDialog->setCancelButton(nullptr);
    _loadingDialog->setMinimumDuration(0);
    _loadingDialog->setValue(99);
}

void MainWindow::hideLoadingDialog()
{
    if (_loadingDialog != nullptr)
    {
        _loadingDialog->setValue(100);
        delete _loadingDialog;
        _loadingDialog = nullptr;
    }
}

void MainWindow::patternLoadedFromServer(const pattern* patt)
{
    SimulationDataParser::applyPatternStructure(_simulationGrid, patt, _antTypesWidgets);

    _ui.rowsTextEdit->setText(QString("%1").arg(patt->height));
    _ui.colsTextEdit->setText(QString("%1").arg(patt->width));

    updateBlackTilesLabel();
    updateIterationText();

    delete patt;
}

void MainWindow::on_applyDimButton_clicked()
{
    updateSimulationGrid();
}

void MainWindow::on_manualSelectBlackTilesButton_clicked()
{
    if (_emplacingAntWidget != nullptr)
        _emplacingAntWidget->setIsEmplacing(false);

    _isEditingBlackTiles = !_isEditingBlackTiles;
    updateManualSelectBlackTilesButtonColor();
}

void MainWindow::on_generateBlackTilesButton_clicked()
{
    if (_ui.blackTilesToGenerate->text().isEmpty() || _ui.blackTilesToGenerate->text().toInt() == 0)
    {
        QMessageBox::critical(this, "Invalid number", "Please enter valid number of black tiles to generate.");
        return;
    }

    int count = _ui.blackTilesToGenerate->text().toInt();
    _simulationGrid->generateBlackTiles(count);
    updateBlackTilesLabel();
}

void MainWindow::on_generateAntsButton_clicked()
{
    std::vector<int> colorsCounts;
    for (unsigned int i = 0; i < _antTypesWidgets->size(); i++)
    {
        int toGenerate = (*_antTypesWidgets)[i]->getCountToGenerate();
        if (toGenerate < 0)
        {
            QMessageBox::critical(this, "Invalid number", "Please enter valid number of ants to generate.");
            return;
        }

        colorsCounts.push_back(toGenerate);
    }

    _simulationGrid->generateAnts(colorsCounts);
}

void MainWindow::on_conflictsCheckbox_clicked()
{
    _simulationGrid->setEverybodyDiesOnConflict(_ui.conflictsCheckbox->isChecked());
}

void MainWindow::on_nextStepButton_clicked()
{
    _simulationGrid->nextStep();
    updateBlackTilesLabel();
}

void MainWindow::on_startButton_clicked()
{
    _simulationGrid->startSimulation();
}

void MainWindow::on_pauseButton_clicked()
{
    _simulationGrid->pauseSimulation();
}

void MainWindow::on_resetButton_clicked()
{
    _simulationGrid->resetSimulation();
    updateBlackTilesLabel();
    updateIterationText();
}

void MainWindow::on_refreshTimeDecButton_clicked()
{
    _simulationGrid->decCurrentRefreshTime();
    updateRefreshTimeText();
}

void MainWindow::on_refreshTimeIncButton_clicked()
{
    _simulationGrid->incCurrentRefreshTime();
    updateRefreshTimeText();
}

void MainWindow::onSocketError(QAbstractSocket::SocketError socketError)
{
    hideLoadingDialog();

    switch (socketError)
    {
    case QAbstractSocket::RemoteHostClosedError:
        break;
    case QAbstractSocket::HostNotFoundError:
        QMessageBox::critical(this, "Connection error",
            "The server was not found. Please check IP/Domain name and port number.");
        break;
    case QAbstractSocket::ConnectionRefusedError:
        QMessageBox::critical(this, "Connection error",
            "The server is unreachable, please try again later.");
        break;
    default:
        QMessageBox::critical(this, "Connection error",
            "Unknown network error occured. Try again later.");
    }
}

void MainWindow::patternSavingDone(const char status)
{
    hideLoadingDialog();

    if (status == RequestSavePattern::STATUS_SUCCESS)
    {
        QMessageBox::information(this, "Pattern saving done",
            "The pattern was successfully stored on the server!");
    }
    else
    {
        QMessageBox::critical(this, "Pattern saving error",
            "Error saving pattern on the server!");
    }
}

void MainWindow::on_actionLoadPattern_file_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this, "Load pattern from file", "", "*.pattern");

    if (fileName.isEmpty())
    {
        QMessageBox::critical(this, "Pattern loading error", "Please choose pattern file");
        return;
    }

    const pattern* patt = LocalFilesHandler::loadPatternFromFile(fileName);

    SimulationDataParser::applyPatternStructure(_simulationGrid, patt, _antTypesWidgets);

    _ui.rowsTextEdit->setText(QString("%1").arg(patt->height));
    _ui.colsTextEdit->setText(QString("%1").arg(patt->width));

    updateBlackTilesLabel();
    updateIterationText();

    QMessageBox::information(this, "Pattern loader", "Pattern from file loaded successfully. Enjoy!");

    delete patt;
}

void MainWindow::on_actionSavePattern_file_triggered()
{
    QString fileName = QFileDialog::getSaveFileName(this, "Save pattern to file", "", "*.pattern");

    if (fileName.isEmpty())
    {
        QMessageBox::critical(this, "Pattern saving error", "Please choose pattern file");
        return;
    }

    const pattern* patt = SimulationDataParser::toPatternStructure(_simulationGrid, _antTypesWidgets);
    LocalFilesHandler::savePatternToFile(fileName, patt);
    delete patt;

    QMessageBox::information(this, "Pattern loader", "Pattern save successfully.");
}

void MainWindow::on_actionQuit_triggered()
{
    QApplication::quit();
}

void MainWindow::on_actionListPatterns_server_triggered()
{
    _listServerPatternsDialog->exec();
}

void MainWindow::on_actionSavePattern_server_triggered()
{
    // Get server file name.
    bool ok;
    QString patternName = QInputDialog::getText(this, "Pattern name", "Pattern name", QLineEdit::Normal, QString(), &ok);

    if (!ok)
        return;

    // Validation
    QRegularExpression re("\\w+");
    QRegularExpressionValidator v(re, 0);
    int pos = 0;

    if (v.validate(patternName, pos) != QValidator::State::Acceptable)
    {
        QMessageBox::critical(this, "Invalid pattern name!", "Use only letters, numbers and '_'.");
        return;
    }

    patternName += ListServerPatternsDialog::PATTERN_FILE_NAME_EXTENSION;

    // Convert simulation data to saving data structure
    const pattern* patt = SimulationDataParser::toPatternStructure(_simulationGrid, _antTypesWidgets);

    // Send save request to the server
    _requestSavePattern->run(
        ServerConfig::instance().getQStringAddress(),
        ServerConfig::instance().getPort(),
        patternName,
        patt
    );

    showLoadingDialog();

    delete patt;
}

void MainWindow::on_actionConfigServer_triggered()
{
    _serverConfigDialog->exec();
}