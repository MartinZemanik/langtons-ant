#include "ServerRequest.h"

ServerRequest::ServerRequest() :
	QObject(),
	_socket(new QTcpSocket(this)),
	_responseBodySize(0)
{
	connect(_socket, &QAbstractSocket::connected, this, &ServerRequest::socketConnected);
	connect(_socket, &QAbstractSocket::errorOccurred, this, &ServerRequest::socketErrorOccured);
	connect(_socket, &QIODevice::readyRead, this, &ServerRequest::incomingDataReady);
}

ServerRequest::~ServerRequest()
{
	_socket->disconnect();
	_socket->deleteLater();
	_socket = nullptr;
}

void ServerRequest::exec(const QString& hostname, quint16 port)
{
	_socket->abort();
	_responseBodySize = getResponseBodySize();
	_socket->connectToHost(hostname, port);
}

qint32 ServerRequest::getResponseBodySize()
{
	return RESPONSE_WITH_VARIABLE_LENGTH;
}

void ServerRequest::incomingDataReady()
{
	// Server is sending response.
	// qDebug() << "SIGNAL: readyRead";	

	// Size of the response is included in the first 4 bytes of the response.
	if (_responseBodySize == RESPONSE_WITH_VARIABLE_LENGTH && _socket->bytesAvailable() >= RESPONSE_DATA_LENGTH_SIZE)
	{
		char responseLength[RESPONSE_DATA_LENGTH_SIZE];
		// Dont need to check bytesRead because bytes should be available.
		_socket->read(responseLength, RESPONSE_DATA_LENGTH_SIZE);
		memcpy(&_responseBodySize, responseLength, RESPONSE_DATA_LENGTH_SIZE);
	}	

	// Size of the response is already known, now wait till whole response is received.
	if (_responseBodySize == 0)
	{
		responseDataReceived(QByteArray());
		_socket->disconnectFromHost();
	}
	else if (_responseBodySize > 0 && _socket->bytesAvailable() >= _responseBodySize)
	{
		// Dont need to check bytesRead because bytes should be available.
		QByteArray responseBody = _socket->read(_responseBodySize);
		responseDataReceived(responseBody);
		_socket->disconnectFromHost();
	}
}

void ServerRequest::socketErrorOccured(QAbstractSocket::SocketError socketError)
{
	// qDebug() << "SIGNAL: errorOccurred";
	emit onSocketError(socketError);
}

void ServerRequest::socketConnected()
{
	// Once connected, send request to the server.
	// qDebug() << "SIGNAL: connected";

	QByteArray requestData = getRequestData();
	int bytesWritten = _socket->write(requestData);

	// 'bytesWritten': -1 or less than all bytes in case of error.
	if (bytesWritten < requestData.size())
	{
		// TODO: Emit error signal.
		qDebug() << ">>> FATAL ERROR: Error writing to socket!";
	}
}

