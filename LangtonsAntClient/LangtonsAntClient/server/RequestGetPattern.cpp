#include "RequestGetPattern.h"

RequestGetPattern::RequestGetPattern() :
	ServerRequest()
{
}

RequestGetPattern::~RequestGetPattern()
{
}

void RequestGetPattern::run(const QString& hostname, quint16 port, const QString& fileName)
{
	_requestBody = QByteArray(fileName.toStdString().c_str(), fileName.size());

	ServerRequest::exec(hostname, port);
}

QByteArray RequestGetPattern::getRequestData()
{
	char requestHeader[ServerRequest::REQUEST_HEADER_SIZE];
	qint32 requestBodySize = _requestBody.size();

	requestHeader[0] = REQUEST_TYPE;
	memcpy(requestHeader + ServerRequest::REQUEST_TYPE_SIZE, &requestBodySize, ServerRequest::REQUEST_DATA_LENGTH_SIZE);

	return QByteArray(requestHeader, ServerRequest::REQUEST_HEADER_SIZE).append(_requestBody);
}

void RequestGetPattern::responseDataReceived(const QByteArray& responseBody)
{
	if (responseBody.size() == 0)
	{
		emit patternGettingDone(false, nullptr);
		return;
	}

	pattern* patt = new pattern;
	memcpy(patt, responseBody.toStdString().c_str(), PATTERN_SIZE);

	emit patternGettingDone(true, patt);
}
