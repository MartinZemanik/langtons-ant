#include "RequestSavePattern.h"

RequestSavePattern::RequestSavePattern() :
	ServerRequest()
{
}

RequestSavePattern::~RequestSavePattern()
{
}

void RequestSavePattern::run(const QString& hostname, quint16 port, const QString& fileName, const pattern* pattern)
{
	_requestBody = QByteArray(fileName.toStdString().c_str(), fileName.size());
	_requestBody.append((const char*) pattern, PATTERN_SIZE);

	ServerRequest::exec(hostname, port);
}

qint32 RequestSavePattern::getResponseBodySize()
{
	return RESPONSE_BODY_SIZE;
}

QByteArray RequestSavePattern::getRequestData()
{
	const quint32 expandedHeaderSize = ServerRequest::REQUEST_TYPE_SIZE + 2 * ServerRequest::REQUEST_DATA_LENGTH_SIZE;
	char expandedRequestHeader[expandedHeaderSize];
	qint32 requestBodySize = ServerRequest::REQUEST_DATA_LENGTH_SIZE + _requestBody.size();
	qint32 fileNameSize = _requestBody.size() - PATTERN_SIZE;
	
	expandedRequestHeader[0] = REQUEST_TYPE;
	memcpy(expandedRequestHeader + ServerRequest::REQUEST_TYPE_SIZE, &requestBodySize, ServerRequest::REQUEST_DATA_LENGTH_SIZE);
	memcpy(expandedRequestHeader + ServerRequest::REQUEST_HEADER_SIZE, &fileNameSize, ServerRequest::REQUEST_DATA_LENGTH_SIZE);

	return QByteArray(expandedRequestHeader, expandedHeaderSize).append(_requestBody);
}

void RequestSavePattern::responseDataReceived(const QByteArray& responseBody)
{
	emit patternSavingDone(responseBody[0]);
}
