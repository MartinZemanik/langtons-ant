#pragma once

#include <QTcpSocket>
#include <QDebug>

class ServerRequest : public QObject
{
    Q_OBJECT

public:
    static const qint32 REQUEST_TYPE_SIZE = sizeof(char);
    static const qint32 REQUEST_DATA_LENGTH_SIZE = sizeof(qint32);
    static const qint32 REQUEST_HEADER_SIZE = REQUEST_TYPE_SIZE + REQUEST_DATA_LENGTH_SIZE;

    static const qint32 RESPONSE_DATA_LENGTH_SIZE = sizeof(qint32);
    static const qint32 RESPONSE_WITH_VARIABLE_LENGTH = -1;

    ServerRequest();
    ~ServerRequest();

protected:
    void exec(const QString& hostname, quint16 port);
    virtual QByteArray getRequestData() = 0;
    virtual qint32 getResponseBodySize();
    virtual void responseDataReceived(const QByteArray& responseBody) = 0;

private:
    QTcpSocket* _socket;
    qint32 _responseBodySize;

private slots:
    void socketConnected();
    void socketErrorOccured(QAbstractSocket::SocketError socketError);
    void incomingDataReady();

signals:
    void onSocketError(QAbstractSocket::SocketError socketError);
};

