#pragma once

#include "ServerRequest.h"
#include <string>
#include <sstream>
#include <QString>
#include <vector>

class RequestFetchFileList : public ServerRequest
{
	Q_OBJECT

public:
	RequestFetchFileList();
	~RequestFetchFileList();
	void run(const QString& hostname, quint16 port);

protected:
	QByteArray getRequestData() override;
	void responseDataReceived(const QByteArray& responseBody) override;

private:
	static const char REQUEST_TYPE = 'l';
	static const char RESPONSE_DELIMETER = ';';

signals:
	void fileListFetched(std::vector<QString> fileList);
};

