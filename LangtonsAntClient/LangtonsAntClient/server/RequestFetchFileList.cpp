#include "RequestFetchFileList.h"

RequestFetchFileList::RequestFetchFileList() :
	ServerRequest()
{
}

RequestFetchFileList::~RequestFetchFileList()
{
}

void RequestFetchFileList::run(const QString& hostname, quint16 port)
{
	ServerRequest::exec(hostname, port);
}

QByteArray RequestFetchFileList::getRequestData()
{
	char requestHeader[ServerRequest::REQUEST_HEADER_SIZE];
	qint32 requestBodySize = 0;

	requestHeader[0] = REQUEST_TYPE;
	memcpy(requestHeader + ServerRequest::REQUEST_TYPE_SIZE, &requestBodySize, ServerRequest::REQUEST_DATA_LENGTH_SIZE);

	return QByteArray(requestHeader, ServerRequest::REQUEST_HEADER_SIZE);
}

void RequestFetchFileList::responseDataReceived(const QByteArray& responseBody)
{
	// qDebug() << "responseDataReceived: " << QString(responseData);
	std::vector<QString> fileList;

	if (responseBody.size() > 0)
	{
		std::string response = responseBody.toStdString();
		std::istringstream ss(response);
		std::string fileName;

		while (std::getline(ss, fileName, RESPONSE_DELIMETER))
			fileList.emplace_back(QString(fileName.c_str()));
	}

	emit fileListFetched(fileList);
}

