#pragma once

#include "ServerRequest.h"
#include "../utilities/structures.h"

class RequestSavePattern : public ServerRequest
{
	Q_OBJECT

public:
	static const char STATUS_SUCCESS = '0';
	static const char STATUS_FAILURE = '1';

	RequestSavePattern();
	~RequestSavePattern();
	void run(const QString& hostname, quint16 port, const QString& fileName, const pattern* pattern);

protected:
	qint32 getResponseBodySize() override;
	QByteArray getRequestData() override;
	void responseDataReceived(const QByteArray& responseBody) override;

private:
	static const char REQUEST_TYPE = 's';
	static const qint32 RESPONSE_BODY_SIZE = 1;
	static const qint32 PATTERN_SIZE = sizeof(pattern);

	QByteArray _requestBody;

signals:
	void patternSavingDone(const char status);
};

