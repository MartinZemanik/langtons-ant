#include "RequestDeletePattern.h"

RequestDeletePattern::RequestDeletePattern() :
	ServerRequest()
{
}

RequestDeletePattern::~RequestDeletePattern()
{
}

void RequestDeletePattern::run(const QString& hostname, quint16 port, const QString& fileName)
{
	_requestBody = QByteArray(fileName.toStdString().c_str(), fileName.size());

	ServerRequest::exec(hostname, port);
}

qint32 RequestDeletePattern::getResponseBodySize()
{
	return RESPONSE_BODY_SIZE;
}

QByteArray RequestDeletePattern::getRequestData()
{
	char requestHeader[ServerRequest::REQUEST_HEADER_SIZE];
	qint32 requestBodySize = _requestBody.size();

	requestHeader[0] = REQUEST_TYPE;
	memcpy(requestHeader + ServerRequest::REQUEST_TYPE_SIZE, &requestBodySize, ServerRequest::REQUEST_DATA_LENGTH_SIZE);

	return QByteArray(requestHeader, ServerRequest::REQUEST_HEADER_SIZE).append(_requestBody);
}

void RequestDeletePattern::responseDataReceived(const QByteArray& responseBody)
{
	emit patternDeletingDone(responseBody[0]);
}
