#pragma once

#include "ServerRequest.h"
#include "../utilities/structures.h"

class RequestGetPattern : public ServerRequest
{
    Q_OBJECT

public:
    RequestGetPattern();
    ~RequestGetPattern();
    void run(const QString& hostname, quint16 port, const QString& fileName);
    static const qint32 PATTERN_SIZE = sizeof(pattern);

protected:
    QByteArray getRequestData() override;
    void responseDataReceived(const QByteArray& responseBody) override;

private:
    static const char REQUEST_TYPE = 'g';
    QByteArray _requestBody;

signals:
    // Param 'patt' is allocated on the heap, handler has to free allocated memory.
    void patternGettingDone(bool found, const pattern* patt);
};

