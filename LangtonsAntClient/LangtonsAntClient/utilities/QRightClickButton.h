#pragma once
#include <QPushButton>
#include <QMouseEvent>

//inspired by https://stackoverflow.com/questions/15658464/qt-rightclick-qpushbutton
class QRightClickButton : public QPushButton
{
    Q_OBJECT

public:
    explicit QRightClickButton(QWidget* parent = 0);

private slots:
    void mousePressEvent(QMouseEvent* e);

signals:
    void clicked();
    void rightClicked();

public slots:

};