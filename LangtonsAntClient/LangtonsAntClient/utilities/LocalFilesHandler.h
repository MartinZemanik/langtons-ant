#pragma once

#include "../utilities/structures.h"
#include <QString>
#include <QDebug>
#include <string>
#include <fstream>
#include <streambuf>
#include <sstream>


class LocalFilesHandler
{
public:
    static bool savePatternToFile(const QString& fileName, const pattern* patt);

    // Caller has to free memory
    static const pattern* loadPatternFromFile(const QString& fileName);
    static const QString PATTERN_FILE_NAME_EXTENSION;

private:
    static const qint32 PATTERN_SIZE = sizeof(pattern);
};