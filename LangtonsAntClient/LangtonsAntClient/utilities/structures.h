#pragma once
#include <string>
#include <QString>

struct ant {
    unsigned char i, j;
    unsigned char antType;
    unsigned char orientation;
};

struct antType
{
    unsigned char priority;
    bool isInverted;
};

struct pattern {
    unsigned char width, height;
    unsigned short antsCount;
    bool isBlack[50][50];
    ant ants[2500];
    antType antTypes[5];
};

inline QString patternToString(const pattern* patt)
{
    std::string str = "************************************************************************\n"
        "Pattern structure:\n";

    // General
    str += " > width: " + std::to_string(patt->width) + "\n";
    str += " > height: " + std::to_string(patt->height) + "\n";
    str += " > antsCount: " + std::to_string(patt->antsCount) + "\n";

    // Black tiles
    str += " > isBlack: [\n";
    for (int i = 0; i < patt->height; i++)
    {
        std::string label = std::to_string(i);
        if (i < 10) label = " " + label;

        str += "   #" + label + ": [";
        for (int j = 0; j < patt->width; j++)
        {
            str += std::to_string(patt->isBlack[i][j] ? 1 : 0) + ", ";
        }
        str += "],\n";
    }
    str += "]\n";

    // Ant types
    str += " > antTypes: [\n";
    for (int i = 0; i < 5; i++)
    {
        str += "   #" + std::to_string(i) + ": {";
        str += "priority: " + std::to_string(patt->antTypes[i].priority) + ", ";
        str += "logic: " + std::to_string(patt->antTypes[i].isInverted ? 1 : 0) + "},\n";
    }
    str += "]\n";

    // Ants
    str += " > ants: [\n";
    for (int i = 0; i < patt->antsCount; i++)
    {
        str += "   #" + std::to_string(i) + ": {";
        str += "i: " + std::to_string(patt->ants[i].i) + ", ";
        str += "j: " + std::to_string(patt->ants[i].j) + ", ";
        str += "colony: " + std::to_string(patt->ants[i].antType) + ", ";
        str += "orientation: " + std::to_string(patt->ants[i].orientation) + "},\n";
    }
    str += "]\n";

    str += "************************************************************************\n";

    return QString(str.c_str());
}