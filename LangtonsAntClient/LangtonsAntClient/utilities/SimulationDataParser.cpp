#include "SimulationDataParser.h"

const pattern* SimulationDataParser::toPatternStructure(const SimulationGrid* grid, const std::vector<AntTypeWidget*>* antTypesWidgets)
{
    pattern* patt = new pattern;
    memset(patt, 0, RequestGetPattern::PATTERN_SIZE);

    patt->width = grid->getColsCount();
    patt->height = grid->getRowsCount();
    patt->antsCount = grid->getCurrentAntsCount();

    for (int i = 0; i < grid->getRowsCount(); i++)
    {
        for (int j = 0; j < grid->getColsCount(); j++)
        {
            patt->isBlack[i][j] = grid->getTile(i, j)->isBlack();
        }
    }

    std::map<AntType*, int> antTypeToIndex;
    for (int i = 0; i < antTypesWidgets->size(); i++)
    {
        antTypeToIndex[(*antTypesWidgets)[i]->getType()] = i;
        patt->antTypes[i].isInverted = (*antTypesWidgets)[i]->getType()->getLogic() == AntLogic::INVERTED;
        patt->antTypes[i].priority = (*antTypesWidgets)[i]->getType()->getPriority();
    }

    const std::vector<Ant*>* ants = grid->getAnts();
    for (int i = 0; i < ants->size(); i++)
    {
        patt->ants[i].i = (*ants)[i]->getI();
        patt->ants[i].j = (*ants)[i]->getJ();
        patt->ants[i].orientation = (*ants)[i]->getRotation();
        patt->ants[i].antType = antTypeToIndex[(*ants)[i]->getType()];
    }

    return patt;
}

void SimulationDataParser::applyPatternStructure(SimulationGrid* grid, const pattern* patt, const std::vector<AntTypeWidget*>* antTypesWidgets)
{
    grid->resizeSimulationGrid(patt->height, patt->width);

    for (int i = 0; i < grid->getRows(); i++)
    {
        for (int j = 0; j < grid->getCols(); j++)
        {
            if (patt->isBlack[i][j])
            {
                grid->getTile(i, j)->setBlackColor();
                grid->setBlackTiles(grid->getCurrentBlackTiles() + 1);
            }
        }
    }

    for (int i = 0; i < patt->antsCount; i++)
    {
        grid->addAnt(
            patt->ants[i].i,
            patt->ants[i].j,
            new Ant(
                patt->ants[i].i,
                patt->ants[i].j,
                patt->ants[i].orientation,
                (*antTypesWidgets)[patt->ants[i].antType]->getType()
            )
        );
    }

    for (unsigned int i = 0; i < antTypesWidgets->size(); i++)
    {
        (*antTypesWidgets)[i]->getType()->setLogic(patt->antTypes[i].isInverted ? AntLogic::INVERTED : AntLogic::NORMAL);
        (*antTypesWidgets)[i]->updateLogicButtonText();

        (*antTypesWidgets)[i]->getType()->setPriority(patt->antTypes[i].priority);
        (*antTypesWidgets)[i]->updatePriorityText();
    }

    grid->updateAntsCounts();
}
