#include "ServerConfig.h"
#include <QString>

ServerConfig::ServerConfig() :
    _address(""),
    _port(0),
    _settings(new QSettings("FRI", "LangtonsAnt"))
{
    loadSettings();
}

ServerConfig::~ServerConfig()
{
    delete _settings;
}

string ServerConfig::getAddress() const
{
    return _address;
}

QString ServerConfig::getQStringAddress() const
{
    return QString::fromStdString(getAddress());
}

quint16 ServerConfig::getPort() const
{
    return _port;
}

QString ServerConfig::getQStringPort() const
{
    return QString::fromStdString(std::to_string(getPort()));
}

bool ServerConfig::isConfiged() const
{
    return !_address.empty() && _port > 0;
}

void ServerConfig::setConfig(string address, quint16 port)
{
    _address = address;
    _port = port;
    saveSettings();
}

void ServerConfig::loadSettings()
{
    _address = _settings->value("server-address", "").toString().toStdString();
    _port = (quint16) _settings->value("server-port", 0).toInt();
}

void ServerConfig::saveSettings()
{
    _settings->setValue("server-address", QString::fromStdString(_address));
    _settings->setValue("server-port", _port);
}

ServerConfig& ServerConfig::instance()
{
    static ServerConfig instance;
    return instance;
}
