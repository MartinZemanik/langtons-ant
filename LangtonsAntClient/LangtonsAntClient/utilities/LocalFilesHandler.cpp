#include "./LocalFilesHandler.h"

const QString LocalFilesHandler::PATTERN_FILE_NAME_EXTENSION = ".pattern";

bool LocalFilesHandler::savePatternToFile(const QString& fileName, const pattern* patt)
{
    std::ofstream patternFile(fileName.toStdString());
    patternFile << std::string((char*)patt, PATTERN_SIZE);
    patternFile.close();

    qDebug() << ">>>>>> " << PATTERN_SIZE;
    qDebug() << patternToString(patt);

    return true;
}

const pattern* LocalFilesHandler::loadPatternFromFile(const QString& fileName)
{
    std::ifstream patternFile(fileName.toStdString());
    std::string patternData((std::istreambuf_iterator<char>(patternFile)), std::istreambuf_iterator<char>());
    patternFile.close();

    qDebug() << patternData.size();

    pattern* patt = new pattern;
    memcpy(patt, patternData.c_str(), PATTERN_SIZE);

    qDebug() << ">>>>>> " << PATTERN_SIZE;
    qDebug() << patternToString(patt);

    return patt;
}
