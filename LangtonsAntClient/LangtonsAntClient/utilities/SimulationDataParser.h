#pragma once
#include <map>
#include <vector>
#include <QString>
#include "../utilities/structures.h"
#include "../simulation/SimulationGrid.h"
#include "../ant/AntTypeWidget.h"
#include "../server/RequestGetPattern.h"

class SimulationDataParser
{
public:
    static const pattern* toPatternStructure(const SimulationGrid* grid, const std::vector<AntTypeWidget*>* antTypesWidgets);
    static void applyPatternStructure(SimulationGrid* grid, const pattern* patt, const std::vector<AntTypeWidget*>* antTypesWidgets);

private:
    SimulationDataParser();
};

