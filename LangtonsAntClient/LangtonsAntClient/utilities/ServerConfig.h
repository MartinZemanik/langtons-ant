#pragma once
#include <QSettings>
#include <string>
using std::string;

class ServerConfig
{
public:
    ~ServerConfig();
    static ServerConfig& instance();

    void setConfig(string address, quint16 port);

    string getAddress() const;
    QString getQStringAddress() const;
    quint16 getPort() const;
    QString getQStringPort() const;
    bool isConfiged() const;

private:
    ServerConfig();

    void loadSettings();
    void saveSettings();

    string _address;
    quint16 _port;

    QSettings* _settings;
};