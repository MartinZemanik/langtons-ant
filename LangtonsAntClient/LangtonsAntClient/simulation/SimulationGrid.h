#pragma once
#include "GridTileWidget.h"
#include "../ant/Ant.h"
#include "../ant/AntTypeWidget.h"
#include <QGridLayout>
#include <QTimer>
#include <QWidget>
#include <vector>
#include <utility>
#include "../utilities/structures.h"

using namespace std;

class SimulationGrid : QWidget
{
    Q_OBJECT

public:
    static const int MAX_SIZE = 50;

    SimulationGrid(QWidget* parent, QGridLayout* gridLayout, std::function<void(int, int)> onTileClick, int simulationSpeed, std::function<void()> updateBlacksCount, std::vector<AntTypeWidget*>* antTypesWidget, std::function<void()> updateIterationText);
    ~SimulationGrid();

    int getRows() const;
    int getCols() const;

    void nextStep();

    void startSimulation();
    void pauseSimulation();
    void resetSimulation();

    int getCurrentRefreshTime() const;
    void incCurrentRefreshTime();
    void decCurrentRefreshTime();

    void resizeSimulationGrid(int rows, int cols);

    int getCurrentIteration() const;
    void setEverybodyDiesOnConflict(bool value);

    void addAnt(int i, int j, Ant* ant);
    void removeAnt(int i, int j);

    GridTileWidget* getTile(int i, int j) const;

    void setBlackTiles(int blackTiles);
    int getCurrentBlackTiles() const;

    void generateBlackTiles(int count);
    void generateAnts(std::vector<int> colorsCounts);
    void updateAntsCounts();

    int getRowsCount() const;
    int getColsCount() const;
    int getCurrentAntsCount() const;
    const std::vector<Ant*>* getAnts() const;

public slots:
    void timerTimeout();

private:
    static const int MIN_REFRESH_TIME = 100;
    static const int MAX_REFRESH_TIME = 2000;
    static const int REFRES_TIME_STEP = 100;

    QWidget* _parent;
    QGridLayout* _gridLayout;
    std::vector<std::vector<GridTileWidget*>>* _gridTiles;
    std::function<void(int, int)> _onTileClick;
    std::function<void()> _updateBlacksCount;
    std::function<void()> _updateIterationText;

    std::vector<Ant*>* _ants;
    std::vector<AntTypeWidget*>* _antTypesWidget;

    int _rows;
    int _cols;

    bool _everybodyDiesOnConflict;
    int _currentBackTiles;

    int _currentIteration;
    int _currentRefreshTime;
    QTimer* _simulationTimer;
};