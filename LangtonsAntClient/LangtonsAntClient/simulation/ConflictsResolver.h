#pragma once
#include "../ant/Ant.h"
#include <vector>
using namespace std;

class ConflictsResolver
{
public:
    static void resolveConflicts(vector<vector<vector<Ant*>>>& ants, bool everybodyDiesOnConflict);

private:
    static Ant* getSurvivor(vector<Ant*> ants);
};