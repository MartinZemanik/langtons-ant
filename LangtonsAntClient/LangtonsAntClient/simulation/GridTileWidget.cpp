#include "GridTileWidget.h"
#include <QBitmap>

GridTileWidget::GridTileWidget(QWidget* parent, std::function<void()> onClick) :
    QWidget(parent),
    _onClick(onClick),
    _currentColor("white"),
    _currentAnt(nullptr)
{
    _tileButton = new QRightClickButton(this);
    connect(_tileButton, SIGNAL(clicked()), this, SLOT(tileClicked()));
    connect(_tileButton, SIGNAL(rightClicked()), this, SLOT(tileRightClicked()));

    setStyleSheet("border: 1px solid grey;");
    setWhiteColor();

    setMaximumSize(TILE_SIZE, TILE_SIZE);
    setMinimumSize(TILE_SIZE, TILE_SIZE);

    _tileButton->setMaximumSize(TILE_SIZE, TILE_SIZE);
    _tileButton->setMinimumSize(TILE_SIZE, TILE_SIZE);
}

GridTileWidget::~GridTileWidget()
{
    delete _tileButton;
}

void GridTileWidget::setBlackColor()
{
    _currentColor = "black";
    updateColor();
}

void GridTileWidget::setWhiteColor()
{
    _currentColor = "white";
    updateColor();
}

void GridTileWidget::invertColor()
{
    if (_currentColor == "black")
        setWhiteColor();
    else
        setBlackColor();
}

bool GridTileWidget::isBlack() const
{
    return _currentColor == "black";
}

bool GridTileWidget::isWhite() const
{
    return _currentColor == "white";
}

void GridTileWidget::setCurrentAnt(Ant* ant)
{
    _currentAnt = ant;
    updateAnt();
}

void GridTileWidget::removeAnt()
{
    setCurrentAnt(nullptr);
}

Ant* GridTileWidget::getCurrentAnt() const
{
    return _currentAnt;
}

void GridTileWidget::updateRotation()
{
    setAntIcon();
}

void GridTileWidget::updateColor()
{
    _tileButton->setStyleSheet(QString("background-color: %1").arg(QString::fromStdString(_currentColor)));
}

void GridTileWidget::updateAnt()
{
    if (_currentAnt != nullptr)
        setAntIcon();
    else
        _tileButton->setIcon(QIcon());
}

void GridTileWidget::setAntIcon()
{
    QPixmap pixmap = QPixmap(QString("icons/ant/ant-%1.png").arg(_currentAnt->getRotation()));
    QBitmap mask = pixmap.createMaskFromColor(Qt::transparent, Qt::MaskInColor);

    pixmap.fill(QColor(_currentAnt->getType()->getColor()));
    pixmap.setMask(mask);

    _tileButton->setIcon(QIcon(pixmap));
}

void GridTileWidget::tileClicked()
{
    _onClick();
}

void GridTileWidget::tileRightClicked()
{
    if (_currentAnt != nullptr)
    {
        _currentAnt->rotateRight();
        setAntIcon();
    }
}