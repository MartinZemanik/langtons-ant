#include "ConflictsResolver.h"
#include <algorithm>

void ConflictsResolver::resolveConflicts(vector<vector<vector<Ant*>>>& ants, bool everybodyDiesOnConflict)
{
    for (unsigned int i = 0; i < ants.size(); i++)
    {
        for (unsigned int j = 0; j < ants[i].size(); j++)
        {
            //conflict
            if (ants[i][j].size() > 1)
            {
                if (everybodyDiesOnConflict)
                {
                    for (int k = 0; k < ants[i][j].size(); k++)
                        delete ants[i][j][k];
                    ants[i][j].clear();
                }
                else
                {
                    Ant* survivor = getSurvivor(ants[i][j]);
                    for (int k = 0; k < ants[i][j].size(); k++)
                    {
                        if (ants[i][j][k] != survivor)
                            delete ants[i][j][k];
                    }
                    ants[i][j].clear();
                    ants[i][j].push_back(survivor);
                }
            }
        }
    }
}

Ant* ConflictsResolver::getSurvivor(vector<Ant*> ants)
{
    std::sort(ants.begin(), ants.end(), [](const Ant* a, const Ant* b)
        {
            return a->getType()->getPriority() < b->getType()->getPriority();
        });

    vector<Ant*> candidates;
    int lowestPriorityFound = numeric_limits<int>::max();
    for (unsigned int i = 0; i < ants.size(); i++)
    {
        if (ants[i]->getType()->getPriority() == lowestPriorityFound)
            candidates.push_back(ants[i]);
        else if (ants[i]->getType()->getPriority() < lowestPriorityFound)
        {
            candidates.clear();
            lowestPriorityFound = ants[i]->getType()->getPriority();

            candidates.push_back(ants[i]);
        }
    }

    return candidates[rand() % candidates.size()];
}
