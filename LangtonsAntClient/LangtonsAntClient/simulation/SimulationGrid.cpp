#include "SimulationGrid.h"
#include "ConflictsResolver.h"
#include <algorithm>
#include <map>

SimulationGrid::SimulationGrid(QWidget* parent, QGridLayout* gridLayout, std::function<void(int, int)> onTileClick, int simulationSpeed, std::function<void()> updateBlacksCount, std::vector<AntTypeWidget*>* antTypesWidget, std::function<void()> updateIterationText) :
    _parent(parent),
    _gridLayout(gridLayout),
    _onTileClick(onTileClick),
    _gridTiles(new std::vector<std::vector<GridTileWidget*>>()),
    _currentBackTiles(0),
    _rows(0),
    _cols(0),
    _ants(new std::vector<Ant*>()),
    _simulationTimer(new QTimer(parent)),
    _currentRefreshTime(simulationSpeed),
    _updateBlacksCount(updateBlacksCount),
    _everybodyDiesOnConflict(false),
    _antTypesWidget(antTypesWidget),
    _updateIterationText(updateIterationText),
    _currentIteration(0)
{
    connect(_simulationTimer, SIGNAL(timeout()), this, SLOT(timerTimeout()));
}

SimulationGrid::~SimulationGrid()
{
    for (unsigned int i = 0; i < _gridTiles->size(); i++)
    {
        for (unsigned int j = 0; j < (*_gridTiles)[i].size(); j++)
            delete (*_gridTiles)[i][j];
    }
    delete _gridTiles;

    for (unsigned int i = 0; i < _ants->size(); i++)
        delete (*_ants)[i];
    delete _ants;
}

int SimulationGrid::getRows() const
{
    return _rows;
}

int SimulationGrid::getCols() const
{
    return _cols;
}

void SimulationGrid::resizeSimulationGrid(int rows, int cols)
{
    resetSimulation();

    if (rows > 0 && rows <= MAX_SIZE && cols > 0 && cols <= MAX_SIZE)
    {
        _rows = rows;
        _cols = cols;
        _currentBackTiles = 0;

        for (unsigned int i = 0; i < _gridTiles->size(); i++)
        {
            for (unsigned int j = 0; j < (*_gridTiles)[i].size(); j++)
            {
                _gridLayout->removeWidget((*_gridTiles)[i][j]);
                delete (*_gridTiles)[i][j];
            }
        }

        _gridTiles->assign(rows, std::vector<GridTileWidget*>(cols, nullptr));

        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < cols; j++)
            {
                (*_gridTiles)[i][j] = new GridTileWidget(_parent, [this, i, j]() {_onTileClick(i, j); });
                _gridLayout->addWidget((*_gridTiles)[i][j], i + 1, j);
            }
        }
    }
}

int SimulationGrid::getCurrentIteration() const
{
    return _currentIteration;
}

void SimulationGrid::setEverybodyDiesOnConflict(bool value)
{
    _everybodyDiesOnConflict = value;
}

void SimulationGrid::addAnt(int i, int j, Ant* ant)
{
    _ants->push_back(ant);
    getTile(i, j)->setCurrentAnt(ant);
}

void SimulationGrid::removeAnt(int i, int j)
{
    Ant* ant = getTile(i, j)->getCurrentAnt();
    _ants->erase(std::find(_ants->begin(), _ants->end(), ant));
    delete ant;
    getTile(i, j)->setCurrentAnt(nullptr);
}

GridTileWidget* SimulationGrid::getTile(int i, int j) const
{
    return (*_gridTiles)[i][j];
}

void SimulationGrid::setBlackTiles(int blackTiles)
{
    _currentBackTiles = blackTiles;
}

int SimulationGrid::getCurrentBlackTiles() const
{
    return _currentBackTiles;
}

void SimulationGrid::generateBlackTiles(int count)
{
    for (unsigned int i = 0; i < _gridTiles->size(); i++)
    {
        for (unsigned int j = 0; j < (*_gridTiles)[i].size(); j++)
            (*_gridTiles)[i][j]->setWhiteColor();
    }

    int generated = 0;
    while (generated < count && generated < _rows * _cols)
    {
        int i = rand() % _rows;
        int j = rand() % _cols;

        if ((*_gridTiles)[i][j]->isWhite())
        {
            (*_gridTiles)[i][j]->setBlackColor();
            generated++;
        }
    }

    _currentBackTiles = generated;
}

void SimulationGrid::generateAnts(std::vector<int> colorsCounts)
{
    for (unsigned int i = 0; i < _ants->size(); i++)
    {
        getTile((*_ants)[i]->getI(), (*_ants)[i]->getJ())->setCurrentAnt(nullptr);
        delete (*_ants)[i];
    }
    _ants->clear();

    for (unsigned int i = 0; i < colorsCounts.size(); i++)
    {
        int generated = 0;
        while (generated < colorsCounts[i] && _ants->size() < _rows * _cols)
        {
            int antI = rand() % _rows;
            int antJ = rand() % _cols;

            if ((*_gridTiles)[antI][antJ]->getCurrentAnt() == nullptr)
            {
                addAnt(antI, antJ, new Ant(antI, antJ, rand() % 4, (*_antTypesWidget)[i]->getType()));
                generated++;
            }
        }

        (*_antTypesWidget)[i]->setCount(generated);
    }
}

void SimulationGrid::updateAntsCounts()
{
    std::map<AntType*, int> antsCounts;
    for (unsigned int i = 0; i < _antTypesWidget->size(); i++)
        antsCounts[(*_antTypesWidget)[i]->getType()] = 0;

    for (Ant* ant : *_ants)
    {
        getTile(ant->getI(), ant->getJ())->setCurrentAnt(ant);
        antsCounts[ant->getType()]++;
    }

    for (unsigned int i = 0; i < _antTypesWidget->size(); i++)
        (*_antTypesWidget)[i]->setCount(antsCounts[(*_antTypesWidget)[i]->getType()]);
}

int SimulationGrid::getRowsCount() const
{
    return _rows;
}

int SimulationGrid::getColsCount() const
{
    return _cols;
}

int SimulationGrid::getCurrentAntsCount() const
{
    return _ants->size();
}

const std::vector<Ant*>* SimulationGrid::getAnts() const
{
    return _ants;
}

void SimulationGrid::nextStep()
{
    if (_ants->empty())
        return;

    for (Ant* ant : *_ants)
    {
        GridTileWidget* antTile = getTile(ant->getI(), ant->getJ());

        if (ant->getType()->getLogic() == AntLogic::NORMAL)
        {
            if (antTile->isWhite())
            {
                ant->rotateRight();
                antTile->setBlackColor();
                _currentBackTiles++;
            }
            else
            {
                ant->rotateLeft();
                antTile->setWhiteColor();
                _currentBackTiles--;
            }
        }
        else
        {
            if (antTile->isWhite())
            {
                ant->rotateLeft();
                antTile->setBlackColor();
                _currentBackTiles++;
            }
            else
            {
                ant->rotateRight();
                antTile->setWhiteColor();
                _currentBackTiles--;
            }
        }

        switch (ant->getRotation())
        {
        case 0:
            ant->moveUp(_rows);
            break;
        case 1:
            ant->moveRight(_cols);
            break;
        case 2:
            ant->moveDown(_rows);
            break;
        case 3:
            ant->moveLeft(_cols);
            break;
        }

        antTile->setCurrentAnt(nullptr);
    }

    vector<vector<vector<Ant*>>> ants(_rows, vector<vector<Ant*>>(_cols, vector<Ant*>()));

    for (Ant* ant : *_ants)
        ants[ant->getI()][ant->getJ()].push_back(ant);

    ConflictsResolver::resolveConflicts(ants, _everybodyDiesOnConflict);

    _ants->clear();
    for (unsigned int i = 0; i < ants.size(); i++)
    {
        for (unsigned int j = 0; j < ants[i].size(); j++)
        {
            for (unsigned int k = 0; k < ants[i][j].size(); k++)
                _ants->push_back(ants[i][j][k]);
        }
    }

    updateAntsCounts();

    _currentIteration++;
    _updateBlacksCount();
    _updateIterationText();
}

void SimulationGrid::startSimulation()
{
    if (_ants->empty())
        return;

    _simulationTimer->start(_currentRefreshTime);
}

void SimulationGrid::pauseSimulation()
{
    _simulationTimer->stop();
}

void SimulationGrid::resetSimulation()
{
    for (unsigned int i = 0; i < _gridTiles->size(); i++)
    {
        for (unsigned int j = 0; j < (*_gridTiles)[i].size(); j++)
            (*_gridTiles)[i][j]->setWhiteColor();
    }
    _currentBackTiles = 0;

    for (unsigned int i = 0; i < _ants->size(); i++)
    {
        getTile((*_ants)[i]->getI(), (*_ants)[i]->getJ())->setCurrentAnt(nullptr);
        delete (*_ants)[i];
    }

    _ants->clear();

    for (unsigned int i = 0; i < _antTypesWidget->size(); i++)
    {
        (*_antTypesWidget)[i]->setCount(0);

        (*_antTypesWidget)[i]->getType()->setLogic(AntLogic::NORMAL);
        (*_antTypesWidget)[i]->updateLogicButtonText();

        (*_antTypesWidget)[i]->getType()->setPriority(0);
        (*_antTypesWidget)[i]->updatePriorityText();
    }

    _currentIteration = 0;
    _simulationTimer->stop();
}

int SimulationGrid::getCurrentRefreshTime() const
{
    return _currentRefreshTime;
}

void SimulationGrid::incCurrentRefreshTime()
{
    if (_currentRefreshTime + REFRES_TIME_STEP <= MAX_REFRESH_TIME)
        _currentRefreshTime += REFRES_TIME_STEP;
}

void SimulationGrid::decCurrentRefreshTime()
{
    if (_currentRefreshTime - REFRES_TIME_STEP >= MIN_REFRESH_TIME)
        _currentRefreshTime -= REFRES_TIME_STEP;
}

void SimulationGrid::timerTimeout()
{
    nextStep();
    _simulationTimer->start(_currentRefreshTime);
}
