#pragma once
#include "../ant/AntLogic.h"
#include "../ant/Ant.h"
#include "../utilities/QRightClickButton.h"
#include <QWidget>
#include <QPushButton>
#include <utility>
#include <string>

class GridTileWidget : public QWidget
{
    Q_OBJECT

public:
    GridTileWidget(QWidget* parent, std::function<void()> onClick);
    ~GridTileWidget();

    void setBlackColor();
    void setWhiteColor();
    void invertColor();

    bool isBlack() const;
    bool isWhite() const;

    void setCurrentAnt(Ant* ant);
    void removeAnt();
    Ant* getCurrentAnt() const;

    void updateRotation();

private slots:
    void tileClicked();
    void tileRightClicked();

private:
    static const int TILE_SIZE = 20;

    void updateColor();
    void updateAnt();
    void setAntIcon();

    QRightClickButton* _tileButton;
    std::function<void()> _onClick;

    std::string _currentColor;
    Ant* _currentAnt;
};