#include "AntType.h"

AntType::AntType(QString color, AntLogic logic, int priority) :
    _color(color),
    _logic(logic),
    _priority(priority)
{
}

QString AntType::getColor() const
{
    return _color;
}

AntLogic AntType::getLogic() const
{
    return _logic;
}

int AntType::getPriority() const
{
    return _priority;
}

void AntType::setLogic(AntLogic logic)
{
    _logic = logic;
}

void AntType::setPriority(int priority)
{
    _priority = priority;
}
