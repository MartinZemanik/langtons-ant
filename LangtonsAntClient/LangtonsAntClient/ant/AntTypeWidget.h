#pragma once
#include "AntLogic.h"
#include "AntType.h"
#include <QWidget>
#include <QGridLayout>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <utility>
#include <QIntValidator>

class AntTypeWidget : public QWidget
{
    Q_OBJECT

public:
    AntTypeWidget(QWidget* parent, QGridLayout* antsLayout, int row, AntType* type, std::function<void(bool)> emplaceButtonClicked);
    ~AntTypeWidget();

    AntType* getType() const;
    int getCountToGenerate() const;
    int getCount() const;

    void setIsEmplacing(bool emplacing);
    void setCount(int count);

    void updateLogicButtonText();
    void updatePriorityText();

private slots:
    void logicButtonClicked();
    void emplaceButtonClicked();
    void onPriorityChanged(const QString& newText);

private:
    void updateEmplaceButton();
    void updateCountText();

    QGridLayout* _antsLayout;

    QPushButton* _colorButton;
    QPushButton* _emplaceButton;
    QPushButton* _logicButton;
    QLabel* _countLabel;
    QLineEdit* _genEdit;
    QLineEdit* _priorityEdit;

    AntType* _type;
    bool _isEmplacing;
    int _count;
    std::function<void(bool)> _emplaceButtonClicked;
};