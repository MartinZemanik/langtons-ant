#include "AntTypeWidget.h"
#include <QPalette>

AntTypeWidget::AntTypeWidget(QWidget* parent, QGridLayout* antsLayout, int row, AntType* type, std::function<void(bool)> emplaceButtonClicked) :
    QWidget(parent),
    _emplaceButtonClicked(emplaceButtonClicked),
    _type(type),
    _isEmplacing(false),
    _count(0),
    _antsLayout(antsLayout)
{
    _colorButton = new QPushButton("", this);

    QFont font = _colorButton->font();
    font.setPointSize(10);

    _colorButton->setFont(font);
    _colorButton->setMinimumWidth(30);
    _colorButton->setMaximumWidth(30);
    _colorButton->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
    _colorButton->setStyleSheet(QString("background-color: %1; border: none; padding: 4px 10px").arg(_type->getColor()));

    _emplaceButton = new QPushButton("", this);
    _emplaceButton->setFont(font);
    _emplaceButton->setMinimumWidth(30);
    _emplaceButton->setMaximumWidth(30);
    _emplaceButton->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
    _emplaceButton->setIcon(QIcon("icons/pencil.png"));
    connect(_emplaceButton, SIGNAL(clicked()), this, SLOT(emplaceButtonClicked()));
    updateEmplaceButton();

    _logicButton = new QPushButton("P", this);
    _logicButton->setFont(font);
    _logicButton->setMinimumWidth(30);
    _logicButton->setMaximumWidth(30);
    _logicButton->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
    _logicButton->setStyleSheet("padding: 4px 10px");
    updateLogicButtonText();
    connect(_logicButton, SIGNAL(clicked()), this, SLOT(logicButtonClicked()));

    _countLabel = new QLabel(this);
    _countLabel->setFont(font);
    updateCountText();

    _genEdit = new QLineEdit("5", this);
    _genEdit->setFont(font);
    _genEdit->setMinimumWidth(50);
    _genEdit->setMaximumWidth(50);
    _genEdit->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
    _genEdit->setPlaceholderText("gen");
    _genEdit->setValidator(new QIntValidator(1, 2500, this));

    _priorityEdit = new QLineEdit("0", this);
    _priorityEdit->setFont(font);
    _priorityEdit->setMinimumWidth(50);
    _priorityEdit->setMaximumWidth(50);
    _priorityEdit->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
    _priorityEdit->setPlaceholderText("priority");
    _priorityEdit->setValidator(new QIntValidator(1, 1000, this));
    connect(_priorityEdit, &QLineEdit::textChanged, this, &AntTypeWidget::onPriorityChanged);

    _antsLayout->addWidget(_colorButton, row, 0);
    _antsLayout->addWidget(_emplaceButton, row, 1);
    _antsLayout->addWidget(_logicButton, row, 2);
    _antsLayout->addWidget(_genEdit, row, 3);
    _antsLayout->addWidget(_priorityEdit, row, 4);
    _antsLayout->addWidget(_countLabel, row, 5);
}

AntTypeWidget::~AntTypeWidget()
{
    delete _colorButton;
    delete _emplaceButton;
    delete _logicButton;
    delete _countLabel;
    delete _genEdit;
    delete _priorityEdit;

    delete _type;
}

AntType* AntTypeWidget::getType() const
{
    return _type;
}

int AntTypeWidget::getCountToGenerate() const
{
    if (_genEdit->text().isEmpty())
        return -1;

    return _genEdit->text().toInt();
}

int AntTypeWidget::getCount() const
{
    return _count;
}

void AntTypeWidget::setIsEmplacing(bool emplacing)
{
    _isEmplacing = emplacing;
    updateEmplaceButton();
}

void AntTypeWidget::setCount(int count)
{
    _count = count;
    updateCountText();
}

void AntTypeWidget::emplaceButtonClicked()
{
    _isEmplacing = !_isEmplacing;
    updateEmplaceButton();

    _emplaceButtonClicked(_isEmplacing);
}

void AntTypeWidget::onPriorityChanged(const QString& newText)
{
    _type->setPriority(newText.toInt());
}

void AntTypeWidget::updateLogicButtonText()
{
    _logicButton->setText(_type->getLogic() == AntLogic::NORMAL ? "N" : "I");
}

void AntTypeWidget::updatePriorityText()
{
    _priorityEdit->setText(QString("%1").arg(_type->getPriority()));
}

void AntTypeWidget::updateEmplaceButton()
{
    QString newButtonColor = _isEmplacing ? "orange" : "white";
    _emplaceButton->setStyleSheet(QString("background-color: %1").arg(newButtonColor));
}

void AntTypeWidget::updateCountText()
{
    _countLabel->setText(QString("#%1").arg(_count));
}

void AntTypeWidget::logicButtonClicked()
{
    _type->setLogic(_type->getLogic() == AntLogic::NORMAL ? AntLogic::INVERTED : AntLogic::NORMAL);
    updateLogicButtonText();
}
