#pragma once
#include "AntType.h"
#include <QString>

class Ant
{
public:
    Ant(int i, int j, int rotation, AntType* type);

    int getI() const;
    int getJ() const;
    int getRotation() const;
    AntType* getType() const;

    void rotateLeft();
    void rotateRight();

    void moveUp(int rows);
    void moveRight(int cols);
    void moveDown(int rows);
    void moveLeft(int cols);

private:
    int _i;
    int _j;
    int _rotation;
    AntType* _type;
};