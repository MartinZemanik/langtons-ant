#pragma once
#include "AntLogic.h"
#include <QString>

class AntType
{
public:
    AntType(QString color, AntLogic logic, int priority);

    QString getColor() const;
    AntLogic getLogic() const;
    int getPriority() const;

    void setLogic(AntLogic logic);
    void setPriority(int priority);

private:
    QString _color;
    AntLogic _logic;
    int _priority;
};