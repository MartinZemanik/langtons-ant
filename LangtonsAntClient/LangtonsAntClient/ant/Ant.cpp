#include "Ant.h"

Ant::Ant(int i, int j, int rotation, AntType* type) :
    _i(i),
    _j(j),
    _rotation(rotation),
    _type(type)
{
}

int Ant::getI() const
{
    return _i;
}

int Ant::getJ() const
{
    return _j;
}

int Ant::getRotation() const
{
    return _rotation;
}

AntType* Ant::getType() const
{
    return _type;
}

void Ant::rotateLeft()
{
    _rotation--;
    if (_rotation < 0)
        _rotation = 3;
}

void Ant::rotateRight()
{
    _rotation++;
    if (_rotation > 3)
        _rotation = 0;
}

void Ant::moveUp(int rows)
{
    _i--;
    if (_i < 0)
        _i = rows - 1;
}

void Ant::moveRight(int cols)
{
    _j++;
    if (_j >= cols)
        _j = 0;
}

void Ant::moveDown(int rows)
{
    _i++;
    if (_i >= rows)
        _i = 0;
}

void Ant::moveLeft(int cols)
{
    _j--;
    if (_j < 0)
        _j = cols - 1;
}

