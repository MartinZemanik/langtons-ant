#ifndef LANGTONSANTSERVER_CONNECTIONSHANDLER_H
#define LANGTONSANTSERVER_CONNECTIONSHANDLER_H

#include <iostream>
#include <unistd.h>
#include <thread>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cstring>
#include <atomic>
#include "RequestHandler.h"

using namespace std;

class ConnectionsHandler
{
private:
    void connectionHandlerThread();

    thread *_thread;
    atomic<bool> _stopWorking;
    int _port;
    int _listenerSocketDescriptor{};
    atomic<int> _activeConnections;

public:
    explicit ConnectionsHandler(int port);

    ~ConnectionsHandler();

    // Start listening to client's requests
    void start();

    // Stop listening to client's requests
    void stop();
};

#endif //LANGTONSANTSERVER_CONNECTIONSHANDLER_H
