#ifndef LANGTONSANTSERVER_INPUTHANDLER_H
#define LANGTONSANTSERVER_INPUTHANDLER_H

#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <cstring>

using namespace std;

class InputHandler
{
private:
    static const int BUFFER_SIZE = 1501;
    static const int REQUEST_TYPE_SIZE = sizeof(char);
    static const int REQUEST_BODY_LENGTH_SIZE = sizeof(int);
    static const int REQUEST_FILE_NAME_LENGTH_SIZE = sizeof(int);
    static const int REQUEST_HEADER_SIZE = REQUEST_TYPE_SIZE + REQUEST_BODY_LENGTH_SIZE;

public:
    static const int READING_ERROR = -1;
    static const int REQUEST_HEADER_READING_ERROR = -2;

    // Return: < 0 -> error, >= 0 -> body size in bytes.
    // Caller has to free memory allocated in 'requestBody' param!
    static int readRequest(int socketDescriptor, char *requestType, char **requestBody);

    // Param 'patternFileName' is out parameter.
    // Param 'patternData' has to be freed by the caller.
    // Return: < 0 -> error, >= -> size of pattern data in bytes.
    static int parseSavePattern(int requestBodySize, char *requestBody, string &patternFileName, char **patternData);
};

#endif //LANGTONSANTSERVER_INPUTHANDLER_H
