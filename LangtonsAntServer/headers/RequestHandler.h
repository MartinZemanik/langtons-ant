#ifndef LANGTONSANTSERVER_REQUESTHANDLER_H
#define LANGTONSANTSERVER_REQUESTHANDLER_H

#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cstring>
#include <unistd.h>
#include <string>
#include <atomic>
#include "FileManager.h"
#include "InputHandler.h"

using namespace std;

class RequestHandler
{
private:
    int _socketDescriptor;
    atomic<int> *_activeConnections;

public:
    RequestHandler(int socketDescriptor, atomic<int> *_activeConnections);

    // Handle receiving of data from client, processing data
    // and sending response back to client
    void operator()();
};

#endif //LANGTONSANTSERVER_REQUESTHANDLER_H
