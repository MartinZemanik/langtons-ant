#ifndef LANGTONSANTSERVER_FILEMANAGER_H
#define LANGTONSANTSERVER_FILEMANAGER_H

#include <string>
#include <iostream>
#include <sys/stat.h>
#include <cstring>
#include <dirent.h>
#include <fstream>
#include <map>
#include <mutex>
#include <cstdio>

using namespace std;

class FileManager
{
private:
    const string PATTERN_DIRECTORY_PATH = "./bestLangtonAntsPatterns";
    static const int INT_SIZE = sizeof(int);

    FileManager();

    map<string, mutex> _mutexes;
public:
    const string RESPONSE_OK = "0";
    const string RESPONSE_ERROR = "1";

    static FileManager &i();

    FileManager(FileManager const &) = delete;

    void operator=(FileManager const &) = delete;

    // Return: according to template = "<size_of_list_4B>nameA.pattern;nameB.pattern;nameC.pattern"
    // size_of_list_4B - int value stored in first 4B
    string getListOfPatterns();

    // Param 'patternFileName' - name of file to get pattern from
    // Return: according to template = "<size_of_pattern_4B><pattern_data>"
    // size_of_pattern_4B - int value stored in first 4B
    string getPattern(const string &patternFileName);

    // Param 'patternData' - data to be saved to file
    // Param 'patternDataSize' - size of data
    // Return: '0' is OK, '1' otherwise
    string savePattern(const string &patternFileName, const char *patternData, int patternDataSize);

    // Param 'patternFileName'
    // Return: '0' is OK, '1' otherwise
    string deletePattern(const string &patternFileName);
};

#endif //LANGTONSANTSERVER_FILEMANAGER_H
