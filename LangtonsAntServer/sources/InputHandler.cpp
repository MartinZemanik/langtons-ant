#include "../headers/InputHandler.h"

int InputHandler::readRequest(int socketDescriptor, char *requestType, char **requestBody)
{
    char buffer[BUFFER_SIZE];
    int bodySize;
    int bodyBytesRead;

    int bytesRead = read(socketDescriptor, buffer, BUFFER_SIZE - 1);

    if (bytesRead <= 0)
        return READING_ERROR;

    if (bytesRead < REQUEST_HEADER_SIZE)
        return REQUEST_HEADER_READING_ERROR;

    // Load part of request from incoming buffer
    *requestType = buffer[0];
    memcpy(&bodySize, buffer + REQUEST_TYPE_SIZE, REQUEST_BODY_LENGTH_SIZE);

    if (bodySize == 0)
        return 0;

    // Determine bodyBytesRead and save data to return buffer
    *requestBody = new char[bodySize];
    bodyBytesRead = bytesRead - REQUEST_HEADER_SIZE;
    memcpy(*requestBody, buffer + REQUEST_HEADER_SIZE, bodyBytesRead);

    // Read until bodySize is reached
    while (bodyBytesRead < bodySize)
    {
        bytesRead = read(socketDescriptor, buffer, BUFFER_SIZE - 1);

        if (bytesRead <= 0)
            return READING_ERROR;

        memcpy(*requestBody + bodyBytesRead, buffer, bytesRead);
        bodyBytesRead += bytesRead;
    }

    return bodySize;
}

int InputHandler::parseSavePattern(int requestBodySize, char *requestBody, string &patternFileName, char **patternData)
{
    int fileNameLength;
    memcpy(&fileNameLength, requestBody, REQUEST_FILE_NAME_LENGTH_SIZE);

    char fileName[fileNameLength];
    memcpy(fileName, requestBody + REQUEST_FILE_NAME_LENGTH_SIZE, fileNameLength);
    patternFileName = string(fileName, fileNameLength);

    int patternDataSize = requestBodySize - REQUEST_FILE_NAME_LENGTH_SIZE - fileNameLength;
    *patternData = new char[patternDataSize];
    memcpy(*patternData, requestBody + REQUEST_FILE_NAME_LENGTH_SIZE + fileNameLength, patternDataSize);

    return patternDataSize;
}
