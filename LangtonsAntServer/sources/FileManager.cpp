#include <unistd.h>
#include "../headers/FileManager.h"

FileManager &FileManager::i()
{
    static FileManager instance;
    return instance;
}

FileManager::FileManager()
{
    // Create a directory if directory not exists
    mkdir(PATTERN_DIRECTORY_PATH.c_str(), 0777);
}

string FileManager::getListOfPatterns()
{
    cout << "FileManager.getListOfPatterns: called.\n";

    string listOfPatterns;

    DIR *patternsDir = opendir(PATTERN_DIRECTORY_PATH.c_str());
    dirent *dp;
    while ((dp = readdir(patternsDir)) != nullptr)
    {
        // Don't include ./ and ../ folders
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {
            listOfPatterns += string(dp->d_name) + ';';
        }
    }
    closedir(patternsDir);

    // Remove last delimiter ';'
    if (!listOfPatterns.empty())
    {
        listOfPatterns = listOfPatterns.substr(0, listOfPatterns.size() - 1);
    }

    // Add length of message to listOfPatterns (see Protocol/protocol.txt)
    int lengthOfMessageInt = listOfPatterns.size();
    char lenOfMessageChar[4];
    memcpy(&lenOfMessageChar, &lengthOfMessageInt, INT_SIZE);

    listOfPatterns = string(lenOfMessageChar, INT_SIZE) + listOfPatterns;

    return listOfPatterns;
}

string FileManager::getPattern(const string &patternFileName)
{
    cout << "FileManager.getPattern: fileName: " << patternFileName << "\n";

    string fileName = PATTERN_DIRECTORY_PATH + "/" + patternFileName;

    _mutexes[fileName].lock();

    ifstream patternFile;
    patternFile.open(fileName);

    // File not exist
    if(!patternFile.good())
    {
        patternFile.close();
        _mutexes[fileName].unlock();

        int lengthOfMessageInt = 0;
        char lenOfMessageChar[4];
        memcpy(lenOfMessageChar, &lengthOfMessageInt, INT_SIZE);

        return string(lenOfMessageChar, INT_SIZE);
    }

    string patternData((std::istreambuf_iterator<char>(patternFile)), std::istreambuf_iterator<char>());
    patternFile.close();

    _mutexes[fileName].unlock();

    // Add length of message to patternData (see Protocol/protocol.txt)
    int lengthOfMessageInt = patternData.size();
    char lenOfMessageChar[4];
    memcpy(lenOfMessageChar, &lengthOfMessageInt, INT_SIZE);

    patternData = string(lenOfMessageChar, INT_SIZE) + patternData;

    return patternData;
}

// If file already exist, function will rewrite it
string FileManager::savePattern(const string &patternFileName, const char *patternData, int patternDataSize)
{
    cout << "FileManager.savePattern: fileName: " << patternFileName << ", data size: " << patternDataSize << "\n";
         // << ", data: " << string(patternData, patternDataSize) << "\n";

    string fileName = PATTERN_DIRECTORY_PATH + "/" + patternFileName;

    _mutexes[fileName].lock();

    ofstream patternFile;
    patternFile.open(fileName);
    patternFile << string(patternData, patternDataSize);
    patternFile.close();

    _mutexes[fileName].unlock();

    // Here no error should occur
    return RESPONSE_OK;
}

string FileManager::deletePattern(const string &patternFileName)
{
    string fileName = PATTERN_DIRECTORY_PATH + "/" + patternFileName;

    _mutexes[fileName].lock();

    if(remove(fileName.c_str()) == 0)
    {
        _mutexes[fileName].unlock();
        return RESPONSE_OK;
    }
    else
    {
        _mutexes[fileName].unlock();
        return RESPONSE_ERROR;
    }
}
