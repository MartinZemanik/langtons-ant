#include "../headers/ConnectionsHandler.h"

ConnectionsHandler::ConnectionsHandler(int port) :
    _stopWorking(false),
    _thread(nullptr),
    _port(port),
    _listenerSocketDescriptor(-1),
    _activeConnections(0)
{

}

ConnectionsHandler::~ConnectionsHandler()
{
    delete _thread;
}

void ConnectionsHandler::start()
{
    _thread = new thread(&ConnectionsHandler::connectionHandlerThread, this);
}

// Called from main thread
void ConnectionsHandler::stop()
{
    cout << "ConnectionHandler.stop: Shutting down.\n";
    _stopWorking = true;

    // Close socket to release from accept()
    shutdown(_listenerSocketDescriptor, SHUT_RDWR);
    close(_listenerSocketDescriptor);

    _thread->join();
}

void ConnectionsHandler::connectionHandlerThread()
{
    int addrLen, newSocketDescriptor;
    sockaddr_in address{};

    // Create listener socket
    if ((_listenerSocketDescriptor = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    {
        cerr << "Socket creation failed\n";
        return;
    }

    // Set listener socket to allow multiple connections
    int opt = 1;
    if (setsockopt(_listenerSocketDescriptor, SOL_SOCKET, SO_REUSEADDR, (char *) &opt,
                   sizeof(opt)) < 0)
    {
        cerr << "Allow multiple connections failed.\n";
        return;
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(_port);

    // Bind listener socket
    if (bind(_listenerSocketDescriptor, (struct sockaddr *) &address, sizeof(address)) < 0)
    {
        cerr << "Bind failed\n";
        return;
    }

    // Set max 5 waiting connections in queue
    if (listen(_listenerSocketDescriptor, 5) < 0)
    {
        cerr << "Listen failed.\n";
        return;
    }

    cout << "ConnectionHandler.start: Started up listening on port " << _port << "\n";
    cout << "Waiting for connections ...\n";

    addrLen = sizeof(address);

    // Until stop() is executed
    while (!_stopWorking)
    {
        // Wait for new client to connect
        if ((newSocketDescriptor = accept(_listenerSocketDescriptor, (struct sockaddr *) &address,
                                          (socklen_t *) &addrLen)) < 0)
        {
            if (_stopWorking)
                break;

            cerr << "Accept new connection failed.\n";
            return;
        }

        cout << "Accept new connection on socket " << newSocketDescriptor << " , ip is : "
             << inet_ntoa(address.sin_addr)
             << " , port is : "
             << ntohs(address.sin_port) << "  \n";

        // New thread
        _activeConnections++;
        thread t(RequestHandler(newSocketDescriptor, &_activeConnections));
        t.detach();
    }

    // Wait for threads to execute
    while (_activeConnections > 0)
    {
        cout << "Waiting for active connections to close: " << _activeConnections << "\n";
        usleep(500);
    }
}
