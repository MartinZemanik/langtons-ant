#include "../headers/RequestHandler.h"

RequestHandler::RequestHandler(int socketDescriptor, atomic<int> *activeConnections) :
        _socketDescriptor(socketDescriptor),
        _activeConnections(activeConnections)
{

}

void RequestHandler::operator()()
{
    char requestType;
    int requestBodySize;
    char *requestBody = nullptr;

    // Read whole request
    requestBodySize = InputHandler::readRequest(_socketDescriptor, &requestType, &requestBody);

    if (requestBodySize < 0)
    {
        cout << "Reading from socket: " << _socketDescriptor << " failed!\n";
        close(_socketDescriptor);

        delete[] requestBody;

        return;
    }

    cout << "Incoming request type: " << requestType << ", size: " << requestBodySize << ", on socket: "
         << _socketDescriptor << "\n";

    string response, patternFileName;
    char *patternData = nullptr;
    int patternDataSize;

    // Handle request according to type of request
    switch (requestType)
    {
        case 'l':
            response = FileManager::i().getListOfPatterns();
            break;
        case 'g':
            response = FileManager::i().getPattern(string(requestBody, requestBodySize));
            break;
        case 's':
            patternDataSize = InputHandler::parseSavePattern(requestBodySize, requestBody, patternFileName,
                                                             &patternData);
            response = FileManager::i().savePattern(patternFileName, patternData, patternDataSize);
            break;
        case 'd':
            response = FileManager::i().deletePattern(string(requestBody, requestBodySize));
            break;
        default:
            response = "x";
            break;
    }

    // Send response to client
    int sendBytes = send(_socketDescriptor, response.c_str(), response.size(), 0);
    cout << "Sent " << sendBytes << " bytes of data\n";

    if (sendBytes != response.size())
        cerr << "Send message size not equal!\n";

    close(_socketDescriptor);

    delete[] patternData;
    delete[] requestBody;

    cout << "Socket " << _socketDescriptor << " successfully disconnected!\n\n";

    (*_activeConnections)--;
}
