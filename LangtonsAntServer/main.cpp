#include <iostream>
#include <mutex>
#include "headers/ConnectionsHandler.h"

#define DEFAULT_PORT 11031

using namespace std;

int main(int argc, char *argv[])
{
    cout << "################################\n";
    cout << "##  Langton's Ant File Server  ##\n";
    cout << "################################\n";

    cout << "\nFor exit send 'q'.\n";

    int port = argc == 2 ? atoi(argv[1]) : DEFAULT_PORT; // NOLINT(cert-err34-c)

    ConnectionsHandler c(port);
    c.start();

    while (true)
    {
        string str;
        cin >> str;
        if (str[0] == 'q')
        {
            c.stop();
            break;
        }
    }

    cout << "Everything closed. Have a nice day.\n";

    return 0;
}
